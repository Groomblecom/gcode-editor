package com.groomble.gcodeeditor.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.groomble.gcodeeditor.Main;

public class MainTest {

	@Test
	public void testMain() {
		Main.main(new String[]{"--hElp"});
		assertEquals(Main.application, null);
		Main.main(new String[]{"-H"});
		assertEquals(Main.application, null);
		Main.main(new String[]{"doesn't even slightly exist"});
		assertNotEquals(Main.application, null);
		assertEquals(Main.application.file, null);
	}

}
