package com.groomble.gcodeeditor.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.groomble.gcodeeditor.VecUtil;

public class VecUtilTest {

	@Test
	public void testSubVecs() {
		float[] out = VecUtil.subVecs(new float[]{1, 4, -32}, new float[]{1, 5, 32, 9283});
		float[] e = new float[]{0, -1, -64};
		for(int i = 0; i < e.length; i++) {
			assertTrue(out[i] == e[i]);
		}
		assertEquals(out.length, 3);
	}

	@Test
	public void testAddVecs() {
		float[] out = VecUtil.addVecs(new float[]{1, 4, -32}, new float[]{1, 5, 32, 9283});
		float[] e = new float[]{2, 9, 0};
		for(int i = 0; i < e.length; i++) {
			assertTrue(out[i] == e[i]);
		}
		assertEquals(out.length, 3);
	}

	@Test
	public void testMultVec() {
		float[] out = VecUtil.multVec(new float[]{1, 4, -32}, -2);
		float[] e = new float[]{-2, -8, 64};
		for(int i = 0; i < e.length; i++) {
			assertTrue(out[i] == e[i]);
		}
		assertEquals(out.length, 3);
	}

}
