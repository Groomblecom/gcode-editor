package com.groomble.gcodeeditor;

public class CoordTranslationCommand extends Command {

	private float[] vec;

	public CoordTranslationCommand(String code, float[] vec) {
		super(code);
		this.vec = vec;
	}

	public float[] translate(float[] commandOffset) {
		float[] out = commandOffset.clone();
		for(int i = 0; i < vec.length; i++) {
			if(!Float.isNaN(vec[i])) {
				out[i] = vec[i];
			}
		}
		return out;
	}

}
