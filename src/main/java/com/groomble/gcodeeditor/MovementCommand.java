package com.groomble.gcodeeditor;

public class MovementCommand extends Command {
	private boolean fastmove;
	private float[] destination;
	private boolean absolute;

	/**
	 * Creates a new movement command
	 * @param destination position (change) represented by this command as a {x, y, z, e} vector; see absolute
	 * @param absolute Represents whether this is an absolute or relative command.
	 * @param fastmove False if this movement uses linear interpolation (results may vary; not all firmware interprets this the same and hardware setups vary)
	 */
	public MovementCommand(String code, float[] destination, boolean absolute, boolean fastmove) {
		super(code, 0);
		this.destination = destination.clone();
		this.absolute = absolute;
		this.fastmove = fastmove;
	}

	/**
	 * Translates a vector by the movement represented by this command.
	 * @param vec a vector to translate in format {x, y, z, e}
	 * @param commandOffset 
	 * @return The input vector translated by the movement this command represents.
	 */
	public float[] apply(float[] vec, float[] commandOffset) {
		if(absolute) {
			float[] out = destination.clone();
			for(int i = 0; i < destination.length; i++) {
				if(Float.isNaN(destination[i]))out[i] = vec[i];
			}
			return VecUtil.addVecs(out, commandOffset);
		}else{
			float[] out = VecUtil.addVecs(vec, destination);
			for(int i = 0; i < destination.length; i++) {
				if(Float.isNaN(destination[i]))out[i] = vec[i];
			}
			return out;
		}
	}

	public boolean isAbsolute() {
		return absolute;
	}

	public boolean isExtruding() {
		return !Float.isNaN(destination[3]) && destination[3] != 0;
	}

	public boolean isMoveInXYPlane() {
		return (!Float.isNaN(destination[0]) && (destination[0] != 0 || absolute)) || (!Float.isNaN(destination[1]) && (destination[1] != 0 || absolute));
	}

	public static String fabLine(float[] otherPos, boolean fastmove) {
		return "G"+(fastmove?0:1)+" X"+otherPos[0]+" Y"+otherPos[1]+" Z"+otherPos[2]+" E"+otherPos[3];
	}
}
