package com.groomble.gcodeeditor;

import java.util.LinkedList;

/**
 * This interface represents a set of changes made to a Program
 * @author groomblecom
 * @see Program#addQueuedOperation(Operation)
 */
public interface Operation {

	/**
	 * Performs some operation on a program without causing 100s of redraw and resimulate events. It is assumed that it starts with the program in a valid state and leaves it in a valid state.
	 * @param p the program to operate on
	 * @param c the modifiable command list to operate on
	 * @returns whether the operation was successful.
	 */
	public boolean perform(Program p, LinkedList<Command> c);
	
	/**
	 * Checks if this Operation is sufficiently important to cause a savepoint
	 * @return true if CTRL+Z should undo this operation by itself, or false if it is a minor operation which should be undone with other operations to prevent huge undo queues.
	 */
	public boolean isUndoPoint();
}
