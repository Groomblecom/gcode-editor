package com.groomble.gcodeeditor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Program {
	private LinkedList<Command> commands;
	private GCodeRunSimulator simulator;
	private ArrayList<ChangeListener> commandsChangeListeners = new ArrayList<ChangeListener>();
	private LinkedList<Operation> operationQueue = new LinkedList<Operation>();
	private ErrorDialogOpener errorDialogOpener;
	
	public Program(LinkedList<Command> commands) {
		this.commands = new LinkedList<Command>(commands);
		this.simulator = new GCodeRunSimulator(this);
	}

	/**
	 * Gets a list of the command objects which form this program.
	 * @return A unmodifiable list which is backed by a LinkedList.
	 */
	public List<Command> getCommands() {
		return Collections.unmodifiableList(commands);
	}
	
	public GCodeRunSimulator getSimulator() {
		return simulator;
	}
	
	public void setCommands(List<Command> l) {
		commands.clear();
		commands.addAll(l);
		fireCommandsChangeListeners();
	}

	public void clearCommands() {
		commands.clear();
		fireCommandsChangeListeners();
	}
	
	protected void fireCommandsChangeListeners() {
		simulator.invalidate(); // _always_ make sure the simulator updates first.
		for(ChangeListener l:commandsChangeListeners) {
			l.stateChanged(new ChangeEvent(this));
		}
	}

	public void addCommandChangeListener(ChangeListener changeListener) {
		commandsChangeListeners.add(changeListener);
	}

	public int numCommands() {
		return commands.size();
	}

	public void addQueuedOperation(Operation operation) {
		operationQueue.offer(operation);
	}

	public void completeOperationQueue() {
		if(operationQueue.size() == 0)return;
		Operation current;
		while((current = operationQueue.poll()) != null) {
			boolean ok = current.perform(this, commands);
			if(!ok && errorDialogOpener != null) {
				errorDialogOpener.openErrorDialog(current instanceof Editor.FillDiscontinuityOperation?"Adding that command would split the program in half; try joining other parts first."
						:current instanceof Editor.ArbitraryAdditionOperation?"This needs a selected command to insert before. \n\nYou may not insert movement commands this way- cut and splice using the other tools."
						:"Operation Failed");
			}
		}
		fireCommandsChangeListeners();
	}

	/**
	 * Sets what object this program will tell when it encounters errors
	 * @param errorDialogOpener the ErrorDialogOpener to set; if null, this is made unset and error will be ignored..
	 */
	public void setErrorDialogOpener(ErrorDialogOpener errorDialogOpener) {
		this.errorDialogOpener = errorDialogOpener;
	}
}
