package com.groomble.gcodeeditor;

public class DiscontinuityMarkerCommand extends Command{

	private DiscontinuityMarkerCommand otherSide;
	private float[] pos;
	protected DiscontinuityMarkerCommand(String code, DiscontinuityMarkerCommand otherSide, Program program, float[] pos) {
		super(code);
		this.otherSide = otherSide;
		this.pos = pos;
	}
	
	public DiscontinuityMarkerCommand getOtherSide() {
		return otherSide;
	}
	
	/**
	 * Convenience method to create a matched pair of markers
	 * @param endPos The ending position to give to the discontinuity
	 * @param startPos The starting position to give to the discontinuity
	 * @param p a program to pass to the DiscontinuityMarkerCommands; however, they are not added to `p`'s command list.
	 * @return a length-2 array of markers, already matched, the element in [0] having the startPos and [1] having the endPos.
	 */
	public static DiscontinuityMarkerCommand[] generatePair(Program p, float[] startPos, float[] endPos) {
		DiscontinuityMarkerCommand a = new DiscontinuityMarkerCommand("; Cut region marker", null, p, startPos);
		a.setOtherSide(new DiscontinuityMarkerCommand("; End cut region marker", a, p, endPos));
		return new DiscontinuityMarkerCommand[]{a, a.getOtherSide()};
	}

	/**
	 * Sets other side to this marker. It does not make this the other side of the passed command.
	 * @param o new other side
	 */
	private void setOtherSide(DiscontinuityMarkerCommand o) {
		otherSide = o;
	}
	/**
	 * Gets the position of this discontinuity in absolute coordinates. This exists so that the renderer & simulator may 'jump' over a cut region instead of trying to stretch commands to fit.
	 * @return the position
	 */
	public float[] getCommandPos() {
		return pos;
		
	}

	/**
	 * This makes a the other side of b and vice versa, and at the same time makes `b.otherSide` the other side of `a.otherSide` and vice versa, thus preserving the integrity of pairings.
	 * @param a The first discontinuity marker (index relationships do not matter, these arguments are arbitrarily ordered)
	 * @param b The second discontinuity marker (index relationships do not matter, these arguments are arbitrarily ordered)
	 */
	public static void makePair(DiscontinuityMarkerCommand a,
			DiscontinuityMarkerCommand b) {
		DiscontinuityMarkerCommand t = a.getOtherSide();
		a.setOtherSide(b);
		DiscontinuityMarkerCommand t2 = b.getOtherSide();
		b.setOtherSide(a);
		t.setOtherSide(t2);
		t2.setOtherSide(t);
	}

	/**
	 * Sets the position where the discontinuity ends/starts
	 * @param newPos a length-4 xyze vector representing the start/stop position 
	 */
	public void setCommandPos(float[] newPos) {
		pos = newPos.clone();
	}
}
