package com.groomble.gcodeeditor;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RectangularShape;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

@SuppressWarnings("serial")
public class Renderer extends JPanel {
	public static final float ZOOM_MAX = 20;
	public static final float ZOOM_MIN = 0.05f;
	public static final float ZOOM_MAX_PERCENT = ZOOM_MAX*100;
	public static final float ZOOM_MIN_PERCENT = ZOOM_MIN*100;
	
	public interface ZoomListener {
		/**
		 * Called whenever the zoom level changes.
		 * @param newZoom The new zoom level, as a percentage.
		 */
		void zoomUpdated(float newZoom);
	}

	private Paint stationaryPaint = new Color(204, 204, 0);
	private float zoom = 1;
	private Paint extrudeMovementPaint = new Color(0, 204, 0);
	private BasicStroke extrudeMovementStroke = new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
	protected float[] renderOffset = new float[2];
	protected ArrayList<ZoomListener> zoomListeners = new ArrayList<ZoomListener>();
	private ArrayList<ChangeListener> filamentRenderWidthListeners = new ArrayList<ChangeListener>();
	private Stroke moveNoExtrudeStroke = new BasicStroke(0.25f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
	private Paint moveNoExtrudePaint = new Color(90, 150, 70);
	private float zMin = -10, zMax = 500;
	private List<Command> commands;
	private GCodeRunSimulator runSimulator;
	private Shape nonMovingShape = new Ellipse2D.Float(-0.75f, -0.75f, 1.5f, 1.5f);
	private Rectangle2D discontinuityShape = new Rectangle2D.Float(-0.5f, -0.5f, 1, 1);
	private Line2D.Float line = new Line2D.Float(); // kept about to avoid instantiation every redraw
	private Program program;
	private ArrayList<Command> selectedList = new ArrayList<Command>();
	private Paint selectedPaint = new Color(0, 0, 204);
	private int iMax = Integer.MAX_VALUE;
	private int iMin = 0;
	private Color markerStroke = new Color(204, 0, 0);
	
	Renderer(Program program) {
		super();
		this.program = program;
		program.addCommandChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				repaint();
			}
			
		});
		this.commands = program.getCommands();
		runSimulator = program.getSimulator();
		addMouseMotionListener(new MouseMotionListener() {
			private int previousMouseY;
			private int previousMouseX;

			@Override
			public void mouseDragged(MouseEvent arg0) {
				renderOffset[0] += arg0.getX()-previousMouseX;
				renderOffset[1] += arg0.getY()-previousMouseY;
				repaint();
				updatePreviousMouse(arg0);
				arg0.consume();
			}

			@Override
			public void mouseMoved(MouseEvent arg0) {
				updatePreviousMouse(arg0);
			}

			private void updatePreviousMouse(MouseEvent e) {
				previousMouseX = e.getX();
				previousMouseY = e.getY();
			}
		});
		addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent arg0) {
				zoom *= Math.pow(1.25, -arg0.getPreciseWheelRotation());
				zoom = (float) Math.min(ZOOM_MAX, Math.max(ZOOM_MIN, zoom));
				for(ZoomListener l: zoomListeners) {
					l.zoomUpdated(zoom*100);
				}
				repaint();
			}
		});
	}
	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D)g;
		Stroke defaultStroke = g2d.getStroke();
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setColor(Color.WHITE);
		g2d.fillRect(0, 0, getWidth(), getHeight());
		g2d.setTransform(doCameraTransform(g2d.getTransform()));
		String bufferedCodes = null;
		float[] pos = null;
		for(Command c:commands) {
			pos = runSimulator.getStartPosition(c);
			if(pos == null) System.out.println("Null Pos in R from line "+c.getLine());
			if(!isVisible(c, pos)) {
				if(bufferedCodes != null) {
					renderConstantScaleString(g2d, bufferedCodes, pos, defaultStroke);
				}
				bufferedCodes = null;
				continue;
			}
			if(c instanceof MovementCommand) {
				MovementCommand m = (MovementCommand)c;
				if(bufferedCodes != null && m.isMoveInXYPlane()) {
					renderConstantScaleString(g2d, bufferedCodes, pos, defaultStroke);
					bufferedCodes = null;
				}
				float[] newPos = runSimulator.getEndPosition(c);
				g2d.setStroke(m.isExtruding()?extrudeMovementStroke:moveNoExtrudeStroke);
				g2d.setPaint(selectedList.contains(c)?selectedPaint :m.isExtruding()?extrudeMovementPaint:moveNoExtrudePaint);
				line.setLine(pos[0], pos[1], newPos[0], newPos[1]);
				g2d.draw(line);
			} else if (c instanceof DiscontinuityMarkerCommand) {
				g2d.setPaint(selectedList.contains(c)?selectedPaint:markerStroke);
				AffineTransform tTransform = g2d.getTransform();
				g2d.translate(pos[0], pos[1]);
				g2d.fill(discontinuityShape);
				g2d.setTransform(tTransform);
			} else if (c != null) {
				g2d.setStroke(defaultStroke);
				g2d.setPaint(selectedList.contains(c)?selectedPaint:stationaryPaint);
				AffineTransform tTransform = g2d.getTransform();
				g2d.translate(pos[0], pos[1]);
				g2d.fill(nonMovingShape);
				g2d.setTransform(tTransform);
				if(bufferedCodes == null) {
					bufferedCodes = c.getCode();
				} else {
					bufferedCodes += ", "+c.getCode();
				}
			}
		}
		if(bufferedCodes != null) {
			renderConstantScaleString(g2d, bufferedCodes, pos, defaultStroke);
		}
		
	}
	
	private boolean isVisible(Command c, float[] pos) {
		if(c == null) {
			return false;
		}
		int index = commands.indexOf(c);
		if(index < iMin  || index > iMax) {
			return false;
		}
		if((pos[2] <= zMin && pos[2] >= zMax)) {
			return false;
		}
		if(c instanceof MovementCommand) {
			float[] newPos = runSimulator.getEndPosition(c);
			if((newPos[2] <= zMin && newPos[2] >= zMax)) {
				return false;
			}
		}
		return true;
	}
	public boolean isVisible(Command c) {
		if(c == null || !commands.contains(c)) return false;
		return isVisible(c, runSimulator.getStartPosition(c));
	}
	
	private void renderConstantScaleString(Graphics2D g2d,
			String string, float[] pos, Stroke stroke) {
		g2d.setStroke(stroke);
		g2d.setPaint(Color.BLACK);
		AffineTransform tTransform = g2d.getTransform();
		g2d.translate(pos[0], pos[1]);
		g2d.scale(1/zoom, 1/zoom);
		g2d.drawString(string, 0, 0);
		g2d.setTransform(tTransform);
	}
	/**
	 * Sets the current zoom level.
	 * @param value zoom level as a percentage greater than zero. Values over 100 are fine.
	 */
	public void setZoom(int value) {
		zoom = value/100f;
		for(ZoomListener l: zoomListeners) {
			l.zoomUpdated(zoom*100);
		}
		repaint();
	}
	/**
	 * Adds a zoom listener
	 * @param e listener to add
	 * @return
	 */
	public boolean addZoomListener(ZoomListener e) {
		return zoomListeners.add(e);
	}
	/**
	 * Removes a zoom listener
	 * @param arg0 Listener to remove
	 */
	public void removeZoomListener(Object arg0) {
		zoomListeners.remove(arg0);
	}
	/**
	 * Gets the lowest z-value to render.
	 * @return the zMin
	 */
	public float getZMin() {
		return zMin;
	}
	/**
	 * Sets the lowest z-value to render.
	 * @param zMin the zMin to set
	 */
	public void setZMin(float zMin) {
		this.zMin = zMin;
		repaint();
	}
	/**
	 * Gets the highest z-value to render.
	 * @return the zMax
	 */
	public float getZMax() {
		return zMax;
	}
	/**
	 * Sets the highest z-value to render.
	 * @param zMax the zMax to set 
	 */
	public void setZMax(float zMax) {
		this.zMax = zMax;
		repaint();
	}
	/**
	 * Sets the highest index to render
	 * @param iMax the index
	 */
	public void setIMax(int iMax) {
		this.iMax = iMax;
		repaint();
	}
	/**
	 * Sets the lowest index to render
	 * @param iMax the index
	 */
	public void setIMin(int iMin) {
		this.iMin = iMin;
		repaint();
	}
	public void setFilamentRenderWidth(float px) {
		extrudeMovementStroke = new BasicStroke(px, extrudeMovementStroke.getEndCap(), extrudeMovementStroke.getLineJoin());
		repaint();
		for(ChangeListener l:filamentRenderWidthListeners) {
			l.stateChanged(new ChangeEvent(this));
		}
	}
	public void addFilamentRenderWidthListener(ChangeListener c) {
		filamentRenderWidthListeners.add(c);
	}
	/**
	 * @return the extrudeMovementStroke
	 */
	public BasicStroke getExtrudeMovementStroke() {
		return extrudeMovementStroke;
	}
	public GCodeRunSimulator getSimulator() {
		return runSimulator;
	}
	public Program getProgram() {
		return program;
	}
	public Stroke getMovementStroke() {
		return moveNoExtrudeStroke;
	}
	public AffineTransform doCameraTransform(AffineTransform t) {
		t.translate(renderOffset[0], renderOffset[1]);
		t.scale(zoom, zoom);
		return t;
	}
	public ArrayList<Command> getSelectedList() {
		return selectedList ;
	}
	/**
	 * Gets the shape currently being used to render discontinuityMarkerCommands
	 * @return a copy of the shape 
	 */
	public RectangularShape getDiscontinuityMarkerShape() {
		return (RectangularShape) discontinuityShape.clone();
	}
}
