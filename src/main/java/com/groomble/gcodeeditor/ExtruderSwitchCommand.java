package com.groomble.gcodeeditor;

public class ExtruderSwitchCommand extends Command {

	private int extruder;

	public ExtruderSwitchCommand(String code, int extruder) {
		super(code);
		this.extruder = extruder;
	}

}
