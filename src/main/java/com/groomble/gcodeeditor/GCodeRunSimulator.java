package com.groomble.gcodeeditor;

import java.util.Hashtable;
import java.util.List;

public class GCodeRunSimulator {
	private List<Command> commands;
	private Hashtable<Command, float[]> positions = new Hashtable<Command, float[]>();
	private boolean ran = false;
	
	public GCodeRunSimulator(Program p) {
		this.commands = p.getCommands();
	}
	
	public synchronized void simulateRun() {
		if(ran == true) {
			return;
		}
		positions.clear();
		float[] pos = new float[4];
		float[] commandOffset = new float[4];
		for(Command c:commands) {
			if(c instanceof MovementCommand) {
				MovementCommand m = (MovementCommand)c;
				float[] newPos = m.apply(pos, commandOffset);
				if(newPos.length != 4)System.out.println("return bad from "+m.getLine());
				pos = newPos;
			} else if(c instanceof DiscontinuityMarkerCommand) { // 'skips' over cut regions to prevent superlong commands which try to bridge the gaps.
				DiscontinuityMarkerCommand d = (DiscontinuityMarkerCommand)c;
				pos = d.getCommandPos();
			} else if(c instanceof CoordTranslationCommand) {
				commandOffset  = ((CoordTranslationCommand)c).translate(commandOffset);
			}
			if(pos == null)System.out.println("Null pos");
			positions.put(c, pos.clone());
		}
		ran = true;
	}

	public synchronized float[] getPos(Command first) {
		if(!ran)simulateRun();
		return positions.get(first);
	}

	public synchronized float[] getStartPosition(Command c) {
		if(!ran)simulateRun();
		float[] r = null;
		if(c == commands.get(0)) {
			r =  new float[4];
		} else if(!(c instanceof MovementCommand)) {
			r = positions.get(c);
		} else {
			r = positions.get(commands.get(commands.indexOf(c)-1));
		}
		return r;
	}
	public synchronized float[] getEndPosition(Command c) {
		if(!ran)simulateRun();
		return positions.get(c);
	}
	public synchronized void invalidate() {
		ran = false;
	}
	
	/**
	 * Calculates how much the extruder moves per mm traveled in the x/y plane.
	 * @param c the command to calculate the ratio for
	 * @return the ratio e/|xyvector| or zero if the command is does not move in the xy plane or if it is not an extruding command.;
	 */
	public float getEPerXYRatio(MovementCommand c) {
		if(!c.isExtruding() || !c.isMoveInXYPlane()) return 0;
		float[] deltaPos = VecUtil.subVecs(getEndPosition(c), getStartPosition(c));
		return (float) (deltaPos[3]/VecUtil.mag(deltaPos, 2));
	}
}
