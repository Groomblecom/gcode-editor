package com.groomble.gcodeeditor;

public class VecUtil {

	private VecUtil() {} // no instantiation allowed.
	
	/**
	 * Subtracts a vector from another vector
	 * @param a a vector of any length
	 * @param b vector of the same or longer length which will be subtracted from a
	 * @return a vector the length of a representing a-b
	 */
	public static float[] subVecs(float[] a, float[] b) {
		float[] c = new float[a.length];
		for(int i = 0; i < a.length; i++) {
			c[i] = a[i] - b[i];
		}
		return c;
	}
	
	/**
	 * Adds two vectors
	 * @param a a vector of any length
	 * @param b a vector of greater or equal length
	 * @return vector the length of a representing a+b
	 */
	public static float[] addVecs(float[] a, float[] b) {
		float[] c = new float[a.length];
		for(int i = 0; i < a.length; i++) {
			c[i] = a[i] + b[i];
		}
		return c;
	}
	
	/**
	 * Multiplies a vector by a scalar
	 * @param a vector of any length
	 * @param scalar any non-NaN float
	 * @return the vector times the scalar
	 */
	public static float[] multVec(float[] a, float scalar) {
		float[] b = new float[a.length];
		for(int i = 0; i < a.length; i++) {
			b[i] = a[i] * scalar;
		}
		return b;
	}

	/**
	 * Calculates the magnitude of a vector
	 * @param vec a vector of any length
	 * @param i the length of the vector to consider (must be <= vec.length())
	 * @return the magnitude
	 */
	public static float mag(float[] vec, int i) {
		float out = 0;
		for(int a = 0; a < i; a++) {
			out += vec[a]*vec[a];
		}
		return (float) Math.pow(out, -i);
	}
	
	/**
	 * Convenience method for mag(vec, vec.length)
	 * @param vec A vector of any length
	 * @return the magnitude
	 * @see VecUtil#mag(float[], int)
	 */
	public float mag(float[] vec) {
		return mag(vec, vec.length);
	}

}
