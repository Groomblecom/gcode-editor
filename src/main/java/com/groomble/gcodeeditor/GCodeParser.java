package com.groomble.gcodeeditor;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Locale;

/**
 * A gcode parser which accepts either a BufferedReader of lines or may simply execute a series of arbitrary gcodes. <br/><br/><b>IMPORTANT:</b> this parser will completely ignore M110 (goto gcode line) commands, treating them as another generic command, which could potentially cause big problems.
 * @author groomblecom
 *
 */
public class GCodeParser extends Thread {
	public interface ProgressCallback {
		void callback(long progress, long total);
		void completed();
	}
	private BufferedReader in;
	private boolean absolute = true;
	private boolean useImperial = false;
	private ProgressCallback callback;
	private long totalSize = 0;
	private long processedSize = 0;
	private LinkedList<Command> commands = new LinkedList<Command>();
	private ErrorDialogOpener errorDialogOpener;

	public LinkedList<Command> getCommands() {
		return commands;
	}

	public void setCommands(LinkedList<Command> commands) {
		this.commands = commands;
	}

	public GCodeParser() {
		in = null;
	}
	
	public GCodeParser(BufferedReader in) {
		this.in = in;
	}
	
	public GCodeParser(BufferedReader in, long totalSize, ProgressCallback callback) {
		this(in);
		this.callback = callback;
		this.totalSize  = totalSize;
	}
	
	@Override
	public void run() {
		String line;
		try {
			while((line = in.readLine()) != null) {
				Command c = parseLine(line);
				if(c != null)commands.offer(c);
				processedSize = line.getBytes().length;
				callback.callback(processedSize, totalSize);
			}
			callback.completed();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				in.close();
			} catch(Exception e) {} //just make sure it's really closed
		}
	}
	
	/**
	 * Parses a single line of gcode using the current state of the parser. It will manipulate the state of the parser; beware that this is absolutely <b>not</b> a pure function.
	 * @param line a single line of gcode
	 * @return a Command object representing the command, or null if the line was empty, a comment, or invalid. Some invalid lines may be treated as valid but irrelevant gcode.
	 */
	public Command parseLine(String line) {
		String[] lineparts = line.split(" ");
		String part = null;
		int i = -1;
		do {
			i++;
			if(i == lineparts.length)return null;
			part = lineparts[i].trim();
		} while(part.isEmpty());
		switch(part.toLowerCase(Locale.ENGLISH).charAt(0)) {
		case ';':
			return null;
		case 'g':
			boolean fastmove = false;
			switch((int)getFloatFromGCode(part)) {
			case 0:
				fastmove = true;
			case 1:
				return new MovementCommand(line, getVecFromGCode(lineparts, i+1, true), absolute, fastmove);
			case 4:
				return new Command(line, (int) getFloatFromGCode(lineparts[i+1]));
			case 20:
				setUseImperial(true);
				return new Command(line);
			case 21:
				setUseImperial(false);
				return new Command(line);
			case 28:
				float[] vec = new float[4];
				for(int b = 0; b < vec.length; b++) {
					vec[b] = Float.NaN; // NaNs are this program's internal representation of "do an absolute move on some axis, but ignore the rest"
				}
				axisfinder:
				for(int b = i+1; b < lineparts.length; b++) {
					if(lineparts[b].trim().length() == 0)continue;
					switch(lineparts[b].trim().toLowerCase(Locale.ENGLISH).charAt(0)) {
					case ';':
						break axisfinder;
					case 'x':
						vec[0] = 0;
						break;
					case 'y':
						vec[1] = 0;
						break;
					case 'z':
						vec[2] = 0;
						break;
					case 'e':
						vec[3] = 0;
						break;
					default:
						System.err.println("Strange G28 command:\n    "+line);
					}
				}
				return new MovementCommand(line, vec, true, false);
			case 90:
				absolute = true;
				return new Command(line);
			case 91:
				absolute = false;
				return new Command(line);
			case 92:
				float[] vec1 = getVecFromGCode(lineparts, i+1, true);
				int nanCount = 0;
				for(float f:vec1) {
					if(Float.isNaN(f)) {
						nanCount++;
					}
				}
				if(nanCount > 3) {
					System.out.println("Setting to origin");
					vec1 = new float[4];
				}
				return new CoordTranslationCommand(line, vec1);
			default:
				System.out.println("Found a rare g-code command:\n    "+line);
				return new Command(line);
			}
		case 't':
			return new ExtruderSwitchCommand(line, (int) getFloatFromGCode(part));
		case 'm':
			switch((int)getFloatFromGCode(part)) {
			case 83:
				absolute = false;
				return new Command(line);
			case 110:
				System.err.println("This program doesn't support M110 commands. The output may be uterly broken.");
				if(errorDialogOpener != null)errorDialogOpener.openErrorDialog("This program doesn't support M110 and has undefined behavior with them. The output may be totally broken.");
				return new Command(line);
			case 126:
			case 127:
			case 129:
				String floatArg = "P0";
				try {
					floatArg = lineparts[i+1];
				} catch (ArrayIndexOutOfBoundsException e) {
					//ignore; optional argument
				}
				return new Command(line, (int)getFloatFromGCode(floatArg));
			case 300:
				int ms = 0;
				for(int b = i+1; b < lineparts.length; b++) {
					if(lineparts[b].toLowerCase(Locale.ENGLISH).charAt(0) == 'p') {
						ms = (int)getFloatFromGCode(lineparts[b]);
						break;
					}
				}
				return new Command(line, ms);
			default:
				return new Command(line);
			}
		default:
			System.out.println("Found a really rare g-code command:\n    "+line);
			return new Command(line);
		}
	}
	/**
	 * Gets the float from a string like "X89.5" or "G92"
	 * @param gcode the string
	 * @return the float
	 */
	public float getFloatFromGCode(String gcode) {
		gcode = gcode.substring(1);
		int commentIndex = gcode.indexOf(';');
		if(commentIndex != -1)gcode = gcode.substring(0, commentIndex);
		return Float.valueOf(gcode);
	}
	/**
	 * Convenience method which assumes that no NaNs are to be added.
	 * @see GCodeParser#getVecFromGCode(String[], int, boolean)
	 */
	public float[] getVecFromGCode(String[] codes, int beginIndex) {
		return getVecFromGCode(codes, beginIndex, false);
	}
	
	/**
	 * Gets a x-y-z-e vector from a gcode formatted command (split by spaces), ignoring non-xyze values. Uses the parser's settings for units; <b>not</b> a pure function.
	 * @param codes a gcode command's arguments, split by spaces
	 * @param beginIndex the index to begin looking from
	 * @param NaNsForMissing If true, put a NaN into the array if there is no declaration of the value
	 * @return a length 4 float[], each value representing x, y, z,and e respectively. Any values not in the GCode are assumed to be zero. Inches will be converted to mm based on GCodeParser.useImperial
	 */
	public float[] getVecFromGCode(String[] codes, int beginIndex, boolean NaNsForMissing) {
		float[] out = new float[4];
		if(NaNsForMissing) {
			for(int i = 0; i < out.length; i++)out[i] = Float.NaN;
		}
		for(int i = beginIndex; i < codes.length; i++) {
			String c = codes[i];
			switch(c.toLowerCase(Locale.ENGLISH).charAt(0)) {
			case 'x':
				out[0] = getFloatFromGCode(c);
				break;
			case 'y':
				out[1] = getFloatFromGCode(c);
				break;
			case 'z':
				out[2] = getFloatFromGCode(c);
				break;
			case 'e':
				out[3] = getFloatFromGCode(c);
				break;
			}
		}
		// All values are internally represented as mm.
		if(useImperial) {
			out = VecUtil.multVec(out, 25.4f);
		}
		return out;
	}
	public boolean isUsingImperial() {
		return useImperial;
	}
	public void setUseImperial(boolean useImperial) {
		this.useImperial = useImperial;
	}

	public void setInputBuffer(BufferedReader bufferedReader, long length) {
		in = bufferedReader;
		totalSize = length;
	}

	public void setCallback(ProgressCallback progressCallback) {
		callback = progressCallback;
	}
	
	public void setErrorDialogOpener(ErrorDialogOpener errorDialogOpener) {
		this.errorDialogOpener = errorDialogOpener;
	}
}
