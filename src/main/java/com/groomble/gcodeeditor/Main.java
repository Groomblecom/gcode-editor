package com.groomble.gcodeeditor;

import java.io.File;
import java.util.Locale;

public class Main {

	public static Application application;
	public static void main(String[] args) {
		File toOpen = null;
		if(args.length > 0) {
			if(args[0].toLowerCase(Locale.ENGLISH).equals("-h") || args[0].toLowerCase(Locale.ENGLISH).equals("--help")) {
				System.out.println("Usage:\n"
						+ "gcodeeditor [filename] [-h]	filename is optional and if present should be the path of a RepRap-Flavored GCode file.\n"
						+ "	-h, --help	show this help message and exit.");
			}
			toOpen = new File(args[0]);
			if(toOpen.canRead() && toOpen.isFile()) {
				if(!toOpen.canWrite()) {
					System.out.println("Read-only file: you will be prompted to save somewhere else");
				}
			} else {
				System.out.println(args[0]+" is not a file or is not accessible.");
				toOpen = null;
			}
		}
		
		if(toOpen != null) {
			new Application(toOpen);
		} else {
			new Application();
		}
	}

}
