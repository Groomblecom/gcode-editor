package com.groomble.gcodeeditor;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Hashtable;
import java.util.LinkedList;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;
import javax.swing.border.AbstractBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.groomble.gcodeeditor.Renderer.ZoomListener;

public class Application {
	@SuppressWarnings("serial")
	private class ScaledImageIcon extends ImageIcon {
		private int w, h;
		
		public ScaledImageIcon(URL resource, int w, int h) {
			super(resource);
			this.w = w;
			this.h = h;
		}
		
		@Override
		public int getIconWidth() {
			return w;
		}

		@Override
		public int getIconHeight() {
			return h;
		}
		
		/**
		 * Only works for graphics2ds, and uses superclass behavior for everything else.
		 */
		@Override
		public void paintIcon(Component c, Graphics g, int x, int y) {
			try {
				Graphics2D g2d = (Graphics2D) g;
				AffineTransform t = g2d.getTransform();
				g2d.translate(x, y);
				double xFactor = ((float)w)/super.getIconWidth();
				double yFactor = ((float)h)/super.getIconHeight();
				g2d.scale(xFactor, yFactor);
				super.paintIcon(c, g2d, 0, 0);
				g2d.setTransform(t);
			} catch(ClassCastException e) {
				super.paintIcon(c, g, x, y);
			}
		}
		
		/**
		 * Returns a low-res version of this icon as an image. Avoid if possible.
		 */
		@Override
		public Image getImage() {
			BufferedImage out = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
			paintIcon(null, out.getGraphics(), 0, 0);
			return out;
		}

	}

	@SuppressWarnings("serial")
	public class FileOpenAction extends AbstractAction {
		
		FileOpenAction() {
			super("Open File");
			putValue(AbstractAction.SMALL_ICON, UIManager.getIcon("FileChooser.upFolderIcon"));
			putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK));
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
			fileChooser.setDialogTitle("Open G-Code File");
			int result = fileChooser.showDialog(jframe, "Open");
			if(result == JFileChooser.APPROVE_OPTION) {
				file = fileChooser.getSelectedFile();
				openFile();
			}
			infoLabel.setText("Drag to pan; zoom if it's too small; click on commands to select; cut them with 'Cut Command'; and don't forget to save!");
		}

	}

	public Icon fileSaveAsIcon = UIManager.getIcon("FileView.directoryIcon");
	
	@SuppressWarnings("serial")
	public class FileSaveAsAction extends AbstractAction {
		
		FileSaveAsAction() {
			super("Save File As");
			putValue(AbstractAction.SMALL_ICON, fileSaveAsIcon );
			putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.SHIFT_DOWN_MASK|InputEvent.CTRL_DOWN_MASK));
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
			fileChooser.setDialogTitle("Save G-Code File");
			int result = fileChooser.showDialog(jframe, "Save");
			if(result == JFileChooser.APPROVE_OPTION) {
				file = fileChooser.getSelectedFile();
				saveFile();
			}
		}

	}
	
	@SuppressWarnings("serial")
	public class FileSaveAction extends AbstractAction {
		
		FileSaveAction() {
			super("Save File");
			putValue(AbstractAction.SMALL_ICON, fileSaveAsIcon);
			putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			saveFile();
		}

	}

	public JFrame jframe;
	private FileOpenAction openFile = new FileOpenAction();
	private FileSaveAsAction saveFileAs = new FileSaveAsAction();
	public File file;
	private JFileChooser fileChooser = new JFileChooser();
	private Renderer renderer;
	private Editor editor;
	private JSlider zoomSlider;
	private JSpinner[] zSpinners;
	
	private Program program;
	private JSlider maxISlider;
	private JSlider minISlider;
	private AbstractAction fillLineAction;
	private JLabel infoLabel;

	@SuppressWarnings("serial")
	Application() {
		System.out.println("Starting main application");
		try { // gotta set the L&F to something, anything other than Metal.
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		
		program = new Program(new LinkedList<Command>());
		program.addCommandChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				fillLineAction.putValue(AbstractAction.SELECTED_KEY, false);
				fillLineAction.actionPerformed(new ActionEvent(jframe, ActionEvent.ACTION_LAST, null));
			}
		});
		program.setErrorDialogOpener(new ErrorDialogOpener() {
			@Override
			public void openErrorDialog(String message) {
				JOptionPane.showMessageDialog(jframe, message, "Error", JOptionPane.ERROR_MESSAGE);
			}
		});
		
		infoLabel = new JLabel("");
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		jframe = new JFrame("GCode Editor");
		jframe.setSize((int) (screenSize.getWidth() - 100),
				(int) (screenSize.getHeight() - 100));
		JMenuBar bar = new JMenuBar();
		JMenu file = new JMenu("File");
		AbstractAction newAction = new AbstractAction("New") {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				program.clearCommands();
			}

		};
		newAction.putValue(AbstractAction.SMALL_ICON, UIManager.getIcon("FileView.fileIcon"));
		newAction.putValue(AbstractAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
		file.add(new JMenuItem(newAction));
		file.add(new JMenuItem(openFile));
		file.add(new JMenuItem(new FileSaveAction()));
		file.add(new JMenuItem(saveFileAs));
		AbstractAction exitAction = new AbstractAction("Exit") {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}

		};
		exitAction.putValue(AbstractAction.SMALL_ICON, UIManager.getIcon("InternalFrame.closeIcon"));
		exitAction.putValue(AbstractAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_DOWN_MASK));
		file.add(new JMenuItem(exitAction));
		bar.add(file);
		
		jframe.setLayout(new BorderLayout());
		renderer = new Renderer(program);
		renderer.addZoomListener(new ZoomListener() {

			@Override
			public void zoomUpdated(float newZoom) {
				zoomSlider.setValue((int) newZoom);
			}
			
		});
		jframe.add(renderer, BorderLayout.CENTER);
		
		editor = new Editor(renderer);
		
		JToolBar toolbar = new JToolBar("Tools", JToolBar.VERTICAL);
		JLabel zoomLabel = new JLabel("Zoom");
		toolbar.add(zoomLabel);
		zoomSlider = new JSlider(JSlider.VERTICAL, (int)Renderer.ZOOM_MIN_PERCENT, (int)Renderer.ZOOM_MAX_PERCENT, 100);
		Hashtable<Integer, JComponent> labels = new Hashtable<Integer, JComponent>();
		labels.put(5, new JLabel("5%"));
		labels.put(100, new JLabel("100%"));
		labels.put(500, new JLabel("500%"));
		labels.put(2000, new JLabel("2,000%"));
		zoomSlider.setLabelTable(labels);
		zoomSlider.setPaintLabels(true);
		zoomSlider.setPaintTicks(true);
		zoomSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {
				renderer.setZoom(zoomSlider.getValue());
			}
			
		});
		toolbar.add(zoomSlider);
		
		zSpinners = new JSpinner[2];
		zSpinners[0] = new JSpinner(new SpinnerNumberModel(renderer.getZMin(), renderer.getZMin(), renderer.getZMax(), 0.5));
		zSpinners[0].setName("From:");
		zSpinners[1] = new JSpinner(new SpinnerNumberModel(renderer.getZMax(), renderer.getZMin(), renderer.getZMax(), 0.5));
		zSpinners[1].setName("To:");
		ChangeListener zSpinnerListener = new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {
				try {
					zSpinners[0].commitEdit();
					zSpinners[1].commitEdit();
				} catch(ParseException e) {
					zSpinners[0].setValue(zSpinners[0].getValue());
					zSpinners[1].setValue(zSpinners[1].getValue());
				}
				float[] vals = new float[2];
				vals[0] = ((Double) zSpinners[0].getValue()).floatValue();
				vals[1] = ((Double) zSpinners[1].getValue()).floatValue();
				renderer.setZMax(Math.max(vals[1], vals[0]));
				renderer.setZMin(Math.min(vals[1], vals[0]));
				infoLabel.setText("These values represent the range of height, in mm, to render");
			}
			
		};
		zSpinners[0].addChangeListener(zSpinnerListener);
		zSpinners[1].addChangeListener(zSpinnerListener);
		toolbar.addSeparator();
		toolbar.add(new JLabel("Draw Z-Range\n"));
		toolbar.add(new JLabel("From: "));
		toolbar.add(zSpinners[0]);
		toolbar.add(new JLabel("To: "));
		toolbar.add(zSpinners[1]);
		
		minISlider = new JSlider(0, 10000, 0);
		maxISlider = new JSlider(0, 10000, 0);
		final Hashtable<Integer, JLabel> maxSliderLabel = new Hashtable<Integer, JLabel>();
		minISlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {
				if(maxISlider.getValue() <= minISlider.getValue())maxISlider.setValue(minISlider.getValue()+1);
				renderer.setIMin(minISlider.getValue());
				maxISlider.setMinimum(minISlider.getValue());
				
				maxISlider.repaint();
			}
			
		});
		MouseMotionAdapter sliderMovementListener = new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				infoLabel.setText("These sliders select commands by index (line number)");
			}
		};
		maxISlider.addMouseMotionListener(sliderMovementListener);
		minISlider.addMouseMotionListener(sliderMovementListener);
		maxISlider.setLabelTable(maxSliderLabel);
		maxISlider.setPaintLabels(true);
		maxISlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {
				renderer.setIMax(maxISlider.getValue());
				maxSliderLabel.clear();
				maxSliderLabel.put(minISlider.getValue(), new JLabel(String.valueOf(minISlider.getValue())));
				maxSliderLabel.put(maxISlider.getMaximum(), new JLabel(String.valueOf(maxISlider.getMaximum())));
				maxISlider.repaint();
			}
			
		});
		maxISlider.setMaximum(10001);
		
		toolbar.addSeparator();
		toolbar.add(new JLabel("Render Indicies"));
		toolbar.add(minISlider);
		toolbar.add(new JLabel("Through"));
		toolbar.add(maxISlider);
		
		toolbar.addSeparator();
		toolbar.add(new JLabel("Filament render width"));
		final JSlider filamentWidthSlider = new JSlider(JSlider.VERTICAL, 1, 12, 4);
		final Hashtable<Integer, JComponent> filamentLabels = new Hashtable<Integer, JComponent>();
		filamentLabels.put(1, new JLabel("0.25mm"));
		filamentLabels.put(12, new JLabel("3mm"));
		final JLabel rovingFilamentLabel = new JLabel("1mm");
		FontMetrics fontMetric = rovingFilamentLabel.getFontMetrics(rovingFilamentLabel.getFont());
		rovingFilamentLabel.setPreferredSize(new Dimension(fontMetric.stringWidth("0.00mm"), fontMetric.getMaxAscent()+fontMetric.getMaxDescent()));
		filamentLabels.put(4, rovingFilamentLabel);
		filamentWidthSlider.addChangeListener(new ChangeListener() {
			private DecimalFormat df = new DecimalFormat("#.##");
			
			@Override
			public void stateChanged(ChangeEvent e) {
				int newWidth = filamentWidthSlider.getValue();
				renderer.setFilamentRenderWidth(newWidth/4f);
				synchronized(rovingFilamentLabel) {
					filamentLabels.values().remove(rovingFilamentLabel);
					if(newWidth != 1 && newWidth != 12) {
						rovingFilamentLabel.setText(df.format(newWidth/4f)+"mm");
						filamentLabels.put(newWidth, rovingFilamentLabel);
					}
				}
				filamentWidthSlider.repaint();
				infoLabel.setText("This adjusts the rendered width in mm of the filament; this is purely cosmetic and does not change the file.");
			}
			
		});
		filamentWidthSlider.setLabelTable(filamentLabels);
		filamentWidthSlider.setPaintLabels(true);
		filamentWidthSlider.setPaintTicks(true);
		toolbar.add(filamentWidthSlider);
		
		JMenu editMenu = new JMenu("Edit"); // Editor JToolbar/Menu from here down
		final JToolBar editorToolbar = new JToolBar("Tools");
		
		final AbstractAction cutAction = new AbstractAction("Cut Command") {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				editor.cutSelected();
				infoLabel.setText("The red blocks are the cut ends; put them back together by adding new movement commands");
			}
			
		};
		URL cutIcon = getClass().getResource("resources/cutIcon.png");
		cutAction.putValue(AbstractAction.SMALL_ICON, new ScaledImageIcon(cutIcon, 16, 16));
		cutAction.putValue(AbstractAction.LARGE_ICON_KEY, new ScaledImageIcon(cutIcon, 32, 32));
		
		cutAction.putValue(AbstractAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_C, 0));
		JButton cutButton = new JButton(cutAction);
		editMenu.add(new JMenuItem(cutAction));
		editorToolbar.add(cutButton);
		
		fillLineAction = new AbstractAction("Add New Movement Command") {
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean selected = (Boolean)getValue(SELECTED_KEY);
				if(selected) {
					putValue(NAME, "Cancel New Movement Command");
					infoLabel.setText("Select a red cut end and then click somewhere to create a new movement command; click on another cut end to finish close the program");
					cutAction.setEnabled(false);
				} else {
					putValue(NAME, "Add New Movement Command");
					infoLabel.setText("Drag to pan; zoom if it's too small; click on commands to select; cut them with 'Cut Command'; and don't forget to save!");
					cutAction.setEnabled(true);
				}
				editor.setSelectingDiscontinuities(selected);
			}
		};
		fillLineAction.putValue(AbstractAction.SELECTED_KEY, false);
		fillLineAction.putValue(AbstractAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_D, 0));
		URL fillLineIcon = getClass().getResource("resources/newCommandIcon.png");
		fillLineAction.putValue(AbstractAction.SMALL_ICON, new ScaledImageIcon(fillLineIcon, 16, 16));
		fillLineAction.putValue(AbstractAction.LARGE_ICON_KEY, new ScaledImageIcon(fillLineIcon, 32, 32));
		editorToolbar.add(new JToggleButton(fillLineAction));
		editMenu.add(new JCheckBoxMenuItem(fillLineAction));
		
		final GCodeParser convenienceParser = new GCodeParser();
		final JTextField toolbarCommandSubmitField = new JTextField("Type new non-movement commands here");
		final AbstractAction addArbitraryCommandAction = new AbstractAction("Add Non-Movement Command") {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Object source = arg0.getSource();
				String line = "; Error inserting command";
				if(source instanceof JTextField) {
					line = ((JTextField)source).getText();
				} else {
					line = toolbarCommandSubmitField.getText();
				}
				Command c = convenienceParser.parseLine(line);
				if(c != null) {
					editor.addArbitraryCommandAtSelected(c);
				}
			}
		};
		URL arbitraryCommandActionIcon = getClass().getResource("resources/newMCommandIcon.png");
		addArbitraryCommandAction.putValue(AbstractAction.SMALL_ICON, new ScaledImageIcon(arbitraryCommandActionIcon, 24, 24));
		addArbitraryCommandAction.putValue(AbstractAction.LARGE_ICON_KEY, new ScaledImageIcon(arbitraryCommandActionIcon, 32, 32));
		final JTextField commandEntryField = new JTextField("Type a command here & press enter to add");
		commandEntryField.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent arg0) {
				commandEntryField.setText("");
			}
			@Override
			public void focusLost(FocusEvent arg0) {
				commandEntryField.setText("Type a command here & press enter to add");
			}
		});
		commandEntryField.setBorder(new CompoundBorder(new AbstractBorder() {
			@Override
			public Insets getBorderInsets(Component c, Insets insets) {
				insets.left = ((Icon) addArbitraryCommandAction.getValue(AbstractAction.SMALL_ICON)).getIconWidth()*2;
				return insets;
			}
			@Override
			public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
				ImageIcon icon = (ImageIcon) addArbitraryCommandAction.getValue(AbstractAction.SMALL_ICON);
				g.setColor(UIManager.getColor("menu"));
				g.fillRect(x, y, icon.getIconWidth()*2, height);
				icon.paintIcon(c, g, x+icon.getIconWidth(), y);
			}
		}, commandEntryField.getBorder()));
		commandEntryField.setAction(addArbitraryCommandAction);
		editMenu.add(commandEntryField);
		toolbarCommandSubmitField.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent arg0) {
				((JTextField) arg0.getSource()).setText("");
			}
			@Override
			public void focusLost(FocusEvent arg0) {
				((JTextField) arg0.getSource()).setText("Type new non-movement commands here");
			}
		});
		toolbarCommandSubmitField.setAction(addArbitraryCommandAction);
		editorToolbar.add(new JButton(addArbitraryCommandAction));
		editorToolbar.add(toolbarCommandSubmitField);
		
		AbstractAction toggleAction = new AbstractAction("Show toolbar") {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				editorToolbar.setVisible((Boolean)getValue(SELECTED_KEY));
			}
		};
		toggleAction.putValue(AbstractAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_T, InputEvent.CTRL_DOWN_MASK));
		toggleAction.putValue(AbstractAction.SELECTED_KEY, true);
		editMenu.add(new JCheckBoxMenuItem(toggleAction));
		
		editorToolbar.add(Box.createHorizontalGlue()); // everything after this is on the right
		editorToolbar.addSeparator();
		
		infoLabel.setText("Open a file (File→Open File) to get started");
		editorToolbar.add(infoLabel);
		editorToolbar.add(Box.createRigidArea(new Dimension(25, 1)));
		
		bar.add(editMenu);
		jframe.setJMenuBar(bar);
		jframe.add(editorToolbar, BorderLayout.SOUTH);
		jframe.add(toolbar, BorderLayout.EAST);
		URL frameIcon = getClass().getResource("resources/GCodeEditorIcon.png");
		try {
			jframe.setIconImage(ImageIO.read(frameIcon));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		jframe.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		jframe.setVisible(true);
		
		fileChooser.setFileFilter(new FileNameExtensionFilter("RepRap G-Code", "g", "gco", "gcode"));
	}

	Application(File toOpen) {
		this();
		file = toOpen;
		openFile();
	}

	private void openFile() {
		try {
			final GCodeParser g = new GCodeParser();
			g.setErrorDialogOpener(new ErrorDialogOpener() {
				
				@Override
				public void openErrorDialog(String message) {
					JOptionPane.showMessageDialog(jframe, "Warning", message, JOptionPane.WARNING_MESSAGE);
				}
			});
			g.setInputBuffer(new BufferedReader(new FileReader(file)), file.length());
					
			g.setCallback(new GCodeParser.ProgressCallback() {

						@Override
						public void callback(long progress, long total) {

						}

						@Override
						public void completed() {
							program.setCommands(g.getCommands());
							int s = program.numCommands();
							maxISlider.setMaximum(s);
							maxISlider.setValue(s);
							minISlider.setMaximum(s-2);
							minISlider.setValue(0);
						}

					});
			g.start();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public void saveFile() {
		if(file == null || (!file.isFile() && file.exists())) {
			JOptionPane.showMessageDialog(jframe, "File is not really a file- perhaps a it is a directory?", "Error", JOptionPane.ERROR_MESSAGE);
			saveFileAs.actionPerformed(null);
			return;
		}
		if(!file.canWrite() && !(file.exists() || file.getParentFile().canWrite())) {
			JOptionPane.showMessageDialog(jframe, "Cannot write to specified file.", "Error", JOptionPane.ERROR_MESSAGE);
			saveFileAs.actionPerformed(null);
			return;
		}
		if(!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(jframe, "Could not create that file.", "Error", JOptionPane.ERROR_MESSAGE);
				saveFileAs.actionPerformed(null);
				return;
			}
		} else {
			int result = JOptionPane.showConfirmDialog(jframe, "Overwrite file \""+file.getPath()+"\"?", "Overwrite?", JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
			if(result == JOptionPane.CANCEL_OPTION)return;
		}
		Writer out = null;
		try {
			out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
			for(Command c:program.getCommands()) {
				if(c != null) {
					out.write(c.getLine());
					((BufferedWriter)out).newLine();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(jframe, "Error", "Failed to save file.", JOptionPane.ERROR_MESSAGE);
		} finally {
			try {
				out.close();
			} catch (Exception e) {
				//Don't care, just close the stream.
			}
		}
	}
	
}
