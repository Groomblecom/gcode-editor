package com.groomble.gcodeeditor;

public class Command {

	private int time;
	private String line;

	/**
	 * Constructs a new Command with everything at defaults and length of time = 0;
	 * @param code the gcode line which generated this command.
	 */
	public Command(String code) {
		this.line = code;
	}

	/**
	 * Constructs a new command which will block for a predetermined minimum of time
	 * @param line the gcode line which generated this command.
	 * @param ms the time to block for in ms
	 */
	public Command(String line, int ms) {
		this(line);
		time = ms;
	}

	public String getCode() {
		return line.split(" ")[0];
	}
	
	public String getLine() {
		return line;
	}

	/**
	 * Creates a GCode Line with a  G92 resetting some or all of the coordinates to something else. Passing four NaNs is equivalent to passing four zeros because of the GCode spec.
	 * @param pos a length-4 XYZE vector. Each component may be any finite value to set the coordinate to or NaN to not set the coordinate
	 * @return A valid line of GCode generated from the position passed in.
	 */
	public static String fabG92Line(float[] pos) {
		StringBuilder out = new StringBuilder("G92");
		char[] prefixes = new char[]{'X', 'Y', 'Z', 'E'};
		for(int i = 0; i < prefixes.length; i++) {
			if(!Float.isNaN(pos[i])) {
				out.append(' ');
				out.append(prefixes[i]);
				out.append(pos[i]);
			}
		}
		return out.toString();
	}
	
}
