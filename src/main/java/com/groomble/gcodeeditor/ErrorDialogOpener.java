package com.groomble.gcodeeditor;

public interface ErrorDialogOpener {
	void openErrorDialog(String message);
}