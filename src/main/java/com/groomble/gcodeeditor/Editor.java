package com.groomble.gcodeeditor;

import java.awt.Cursor;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

public class Editor implements MouseListener, ChangeListener {

	/**
	 * An operation for inserting a single (non-moving) command before an element
	 * @author Groomblecom
	 *
	 */
	public class ArbitraryAdditionOperation implements Operation {

		private Command before;
		private Command toAdd;

		/**
		 * Constructs a new operation which will add the toAdd element before the 0th entry in selected. The 0th entry is computed here, but 'before' in computed every time this operation is performed.
		 * @param selected a non-empty, non-null ArrayList.
		 * @param c the command to add.
		 */
		public ArbitraryAdditionOperation(ArrayList<Command> selected, Command c) {
			if(selected.isEmpty()) {
				before = null;
			} else {
				before = selected.get(0);
			}
			toAdd = c;
		}

		@Override
		public boolean perform(Program p, LinkedList<Command> c) {
			if(before == null || toAdd instanceof MovementCommand || toAdd.getCode().toLowerCase().equals("G1") || toAdd.getCode().toLowerCase().equals("G0")) {
				return false;
			}
			int index = c.indexOf(before);
			c.add(index, toAdd);
			return true;
		}

		@Override
		public boolean isUndoPoint() {
			return true;
		}

	}

	/**
	 * Inserts a single absolute MovementCommand to the specified position and adds a G92 to reset the extruder's coordinate to prevent odd side effects.
	 * @author groomblecom
	 *
	 */
	public class InsertSingleMovementOperation implements Operation {

		private int index;
		private float[] newPos;
		private float[] oldPos;
		private MovementCommand lastMoveCommand;

		/**
		 * Constructs (but does not execute) a new InsertSingleMovementOperation
		 * @param insertIndex the index to insert the MovementCommand
		 * @param newPos the float[4] position to move to. This class will ignore the E value in favor of computing the correct E value (based on prior MovementCommand's E/mm ratio).
		 * @param oldPos the float[4] position to move from. Used to compute the E value. 
		 */
		public InsertSingleMovementOperation(int insertIndex, float[] newPos, float[] oldPos) {
			index = insertIndex;
			this.newPos = newPos.clone();
			this.oldPos = oldPos==null?null:oldPos.clone();
		}
		
		@Override
		public boolean perform(Program p, LinkedList<Command> c) {
			float eRatio = 0;
			float[] finalPos = newPos.clone();
			ListIterator<Command> iter = c.listIterator(index);
			Command m;
			while(iter.hasPrevious()) {
				if((m = iter.previous()) instanceof MovementCommand) {
					eRatio = program.getSimulator().getEPerXYRatio((MovementCommand) m);
					if(eRatio != 0) {
						break;
					}
				}
			}
			finalPos[3] += eRatio*VecUtil.mag(VecUtil.subVecs(oldPos, newPos), 2);
			float NaN = Float.NaN;
			Command G92Command = new Command(Command.fabG92Line(new float[]{NaN, NaN, NaN, newPos[3]}));
			c.add(index, G92Command);
			lastMoveCommand = new MovementCommand(MovementCommand.fabLine(finalPos, false), finalPos, true, false);
			c.add(index, lastMoveCommand);
			return true;
		}

		@Override
		public boolean isUndoPoint() {
			return false;
		}

		/**
		 * Gets the last added movement command.
		 * @return the MovementCommand last added. This command will be an absolute movement (unless a subclass overrides perform()).
		 */
		public MovementCommand getLastAddedMovementCommand() {
			return lastMoveCommand;
		}

	}

	public class FillDiscontinuityOperation implements Operation {
		private DiscontinuityMarkerCommand a, b;
		private ArrayList<Command> commandList = new ArrayList<Command>(); // kept about to avoid reallocating
		
		public FillDiscontinuityOperation(DiscontinuityMarkerCommand command, DiscontinuityMarkerCommand command2) {
			a = command;
			b = command2;
		}

		@Override
		public boolean perform(Program p, LinkedList<Command> c) {
			DiscontinuityMarkerCommand oldAOtherSide = a.getOtherSide();
			commandList.clear();
			int iA = c.indexOf(a);
			int iB = c.indexOf(b);
			int lowerIndex = Math.min(iA, iB);
			int higherIndex = Math.max(iA, iB);
			DiscontinuityMarkerCommand higherCommand = iA>iB?a:b;
			DiscontinuityMarkerCommand lowerCommand = iA<=iB?a:b;
			ListIterator<Command> iter;
			if(a.getOtherSide() != b) {
				DiscontinuityMarkerCommand.makePair(a, b);
				iter = c.listIterator(lowerIndex+1);
				outer:
				while(iter.nextIndex() < higherIndex) { // check to make sure this is an allowed op and will not orphan commands
					Command cur = iter.next();
					if(cur instanceof DiscontinuityMarkerCommand) {
						for(int i = 0; i < commandList.size(); i++) {
							Command bcur = commandList.get(i);
							if(((DiscontinuityMarkerCommand) bcur).getOtherSide() == cur) {
								commandList.remove(i);
								continue outer;
							}
						}
						commandList.add(cur);
					}
				}
				iter = null;
				if(commandList.isEmpty()) {
					DiscontinuityMarkerCommand.makePair(a, oldAOtherSide);
					System.out.println("Operation would cut off loop");
					return false;
				}
				commandList.clear();
				iter = c.listIterator(lowerIndex+1);
				while(iter.nextIndex() <= higherIndex) {
					commandList.add(iter.next());
				}
				int i = commandList.size()-1;
				while(iter.previousIndex() > lowerIndex) {
					iter.set(commandList.get(i));
					i--;
					iter.previous();
				}
			}
			p.addQueuedOperation(new InsertSingleMovementOperation(lowerIndex, higherCommand.getCommandPos(), lowerCommand.getCommandPos()));
			c.remove(a);
			c.remove(b);
			
			return true;
		}

		@Override
		public boolean isUndoPoint() {
			return true;
		}

	}

	public class CuttingOperation implements Operation {

		private ArrayList<Command> selected;

		public CuttingOperation(ArrayList<Command> selected) {
			this.selected = new ArrayList<Command>(selected);
			selected.clear();
		}

		@Override
		public boolean perform(Program p, LinkedList<Command> c) {
			if(c.isEmpty() || selected.isEmpty())return true;
			int lowerIndex = Integer.MAX_VALUE;
			int higherIndex = Integer.MIN_VALUE;
			float[] startPos = null;
			float[] endPos = null;
			for(Command rm: selected) {
				int index = c.indexOf(rm);
				if(lowerIndex > index) {
					startPos = p.getSimulator().getStartPosition(rm);
					lowerIndex = index;
				}
				if(higherIndex < c.indexOf(rm)) {
					endPos = p.getSimulator().getEndPosition(rm);
					higherIndex = index;
				}
				c.remove(rm);
			}
			Command lower = c.get(lowerIndex-1);
			Command higher = c.get(lowerIndex);
			if(lower instanceof DiscontinuityMarkerCommand) {
				if(higher instanceof DiscontinuityMarkerCommand) { // joining two discontinuity pairs
					DiscontinuityMarkerCommand.makePair(((DiscontinuityMarkerCommand) lower).getOtherSide(),
							((DiscontinuityMarkerCommand) higher).getOtherSide());
					c.remove(lower);
					c.remove(higher);
				} else {
					((DiscontinuityMarkerCommand) lower).setCommandPos(endPos);
				}
			} else if(higher instanceof DiscontinuityMarkerCommand) {
				((DiscontinuityMarkerCommand) higher).setCommandPos(startPos);
			} else {
				DiscontinuityMarkerCommand[] pair = DiscontinuityMarkerCommand.generatePair(p, startPos, endPos);
				c.add(lowerIndex, pair[1]);
				c.add(lowerIndex, pair[0]);
			}
			return true;
		}

		@Override
		public boolean isUndoPoint() {
			return true;
		}

	}

	@SuppressWarnings("serial")
	private class ClarificationAction extends AbstractAction {
		private Command command;
		ClarificationAction(Command command) {
			super(command.getLine());
			new JMenuItem(this);
			clarifyMenu.add(this);
			this.command = command;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			clarifyMenu.removeAll();
			selectCommand(command);
		}

	}

	private Renderer renderer;
	private Hashtable<Shape, Command> commandShapes;
	private Hashtable<Shape, Command> markerCommandShapes;
	private GCodeRunSimulator simulator;
	private Program program;
	private JPopupMenu clarifyMenu;
	private ArrayList<Command> selected;
	private boolean selectingDiscontinuities = false;
	private final static ArrayList<Shape> potentialSelectionList = new ArrayList<Shape>(); // kept about to avoid allocating over and over
	
	/**
	 * Creates a new Editor object which will use the Program and operate on the selectedList of the passed renderer.
	 * @param renderer
	 */
	public Editor(Renderer renderer) {
		this.renderer = renderer;
		selected = renderer.getSelectedList();
		program = renderer.getProgram();
		simulator = program.getSimulator();
		program.addCommandChangeListener(this);
		clarifyMenu = new JPopupMenu("Clarify Selection:");
		clarifyMenu.addPopupMenuListener(new PopupMenuListener() {

			@Override
			public void popupMenuCanceled(PopupMenuEvent arg0) {
				clarifyMenu.removeAll();
				clarifyMenu.pack();
			}

			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent arg0) {
			}

			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent arg0) {
				clarifyMenu.add(new JLabel("Clarify Selection:"), 0);
			}
		});
		renderer.addMouseListener(this);
		renderer.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
		commandShapes = new Hashtable<Shape, Command>();
		markerCommandShapes = new Hashtable<Shape, Command>();
		for(Command c:program.getCommands()) {
			generateShape(c);
		}
	}

	public void selectCommand(Command command) {
		if(selectingDiscontinuities && !selected.isEmpty()) {
			program.addQueuedOperation(new FillDiscontinuityOperation((DiscontinuityMarkerCommand)command, (DiscontinuityMarkerCommand)selected.get(0)));
			selected.clear();
			program.completeOperationQueue();
		} else {
			selected.clear();
			selected.add(command);
		}
		renderer.repaint();
	}

	/**
	 * Generates a shape for a command and stores it in the selection lookup table.
	 * @param c the command to process
	 */
	private void generateShape(Command c) {
		commandShapes.values().remove(c);
		markerCommandShapes.values().remove(c);
		Shape s;
		if(c instanceof MovementCommand) {
			float[] start = simulator.getStartPosition(c);
			float[] end = simulator.getEndPosition(c);
			Line2D line = new Line2D.Float(start[0], start[1], end[0], end[1]);
			Stroke stroke = ((MovementCommand)c).isExtruding()?renderer.getExtrudeMovementStroke():renderer.getMovementStroke();
			s = stroke.createStrokedShape(line);
		} else if(c instanceof DiscontinuityMarkerCommand) {
			Rectangle2D r = (Rectangle2D) renderer.getDiscontinuityMarkerShape();
			float[] cPos = ((DiscontinuityMarkerCommand) c).getCommandPos();
			r.setRect(cPos[0]-r.getWidth()/2, cPos[1]-r.getHeight()/2, r.getWidth(), r.getHeight());
			markerCommandShapes.put(r, (DiscontinuityMarkerCommand) c);
			return; // Don't select DiscontinuityMarkerCommands normally; only select them from this table
		} else {
			float[] pos = simulator.getEndPosition(c);
			s = new Ellipse2D.Float(pos[0]-0.75f, pos[1]-0.75f, 1.5f, 1.5f);
		}
		commandShapes.put(s, c);
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		AffineTransform cameraTransform = renderer.doCameraTransform(new AffineTransform());
		try {
			cameraTransform.invert();
		} catch (NoninvertibleTransformException e) {
			e.printStackTrace();
		}
		Point2D clickLocation = new Point2D.Double(arg0.getX(), arg0.getY());
		cameraTransform.transform(clickLocation, clickLocation);
		System.out.println("Running selection routine");
		
		potentialSelectionList.clear();
		Hashtable<Shape, Command> currentTable = selectingDiscontinuities?markerCommandShapes:commandShapes;
		for(Shape s:currentTable.keySet()) {
			if(!renderer.isVisible(currentTable.get(s))) {
				continue;
			}
			if(s.contains(clickLocation)) {
				potentialSelectionList.add(s);
			}
		}
		if(potentialSelectionList.size() > 1) {
			for(Shape s:potentialSelectionList) {
				new ClarificationAction(currentTable.get(s));
			}
			clarifyMenu.show(renderer, arg0.getX(), arg0.getY());
		} else if(potentialSelectionList.size() == 1) {
			System.out.println("Selected single command "+program.getCommands().indexOf(currentTable.get(potentialSelectionList.get(0))));
			selectCommand(currentTable.get(potentialSelectionList.get(0)));
		} else if(potentialSelectionList.isEmpty()) {
			if(selectingDiscontinuities && !selected.isEmpty()) {
				List<Command> commands = program.getCommands();
				final DiscontinuityMarkerCommand select = (DiscontinuityMarkerCommand) selected.get(0);
				final float[] oldPos = select.getCommandPos();
				final float[] newPos = new float[]{(float) clickLocation.getX(), (float) clickLocation.getY(), oldPos[2], oldPos[3]};
				boolean higherCommand = commands.indexOf(select)>commands.indexOf(select.getOtherSide());
				final InsertSingleMovementOperation movementOperation = new InsertSingleMovementOperation(commands.indexOf(select)+(higherCommand?1:0),
						higherCommand?oldPos:newPos, higherCommand?newPos:oldPos); // flips positions when building 'backwards', from high to low.
				program.addQueuedOperation(movementOperation);
				program.addQueuedOperation(new Operation() {
					@Override
					public boolean perform(Program p, LinkedList<Command> c) {
						select.setCommandPos(newPos);
						return true;
					}
					@Override
					public boolean isUndoPoint() {
						return false;
					}
				});
				program.completeOperationQueue();
			} else { 
				System.out.println("didn't select anything");
				selected.clear();
				renderer.repaint();
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		commandShapes.clear();
		for(Command c:program.getCommands()) {
			generateShape(c);
		}
	}

	public void cutSelected() {
		program.addQueuedOperation(new CuttingOperation(selected));
		program.completeOperationQueue();
	}

	/**
	 * Queues and runs a operation to insert the command before the selection. 
	 * @param c the non-movement command to insert.
	 */
	public void addArbitraryCommandAtSelected(Command c) {
		program.addQueuedOperation(new ArbitraryAdditionOperation(selected, c));
		program.completeOperationQueue();
	}
	
	/**
	 * Sets whether this editor is selecting discontinuityMarkerCommands, or selecting other types of commands
	 * @param isSelecting true if selecting discontinuities, false otherwise.
	 */
	public void setSelectingDiscontinuities(boolean isSelecting) {
		selectingDiscontinuities = isSelecting;
		selected.clear();
		renderer.repaint();
	}
}
