#!/bin/bash
VERSION=$1
if [[ "$VERSION" != *-* ]]
then
	VERSION=$VERSION-1
fi
mkdir "target/gcodeeditor$VERSION"
cd target/gcodeeditor$VERSION
mkdir -p usr/local/bin
cp ../gcodeeditor.jar usr/local/bin/gcodeeditor
mkdir -p DEBIAN
mkdir -p usr/share/applications
echo "[Desktop Entry]
Version=$VERSION
Name=GCode Editor
Comment=A RepRap-flavored GCode Editor for tweaking the output of a slicer program
Exec=/usr/local/bin/gcodeeditor %u
Icon=/usr/share/gcodeeditor/icon.png
MimeType=text/x-gcode
Terminal=false
Type=Application
Categories=Utility;Application;" > usr/share/applications/gcodeeditor.desktop
mkdir -p usr/share/mime/packages
echo '<?xml version="1.0"?>
<mime-info xmlns="http://www.freedesktop.org/standards/shared-mime-info">
  <mime-type type="text/x-gcode">  
  <comment>RepRap-flavored GCode program</comment>
  <glob pattern="*.gcode"/>
 </mime-type>
</mime-info>
' > usr/share/mime/packages/gcodeeditor.xml

echo "#!/bin/sh
sudo update-mime-database /usr/share/mime
" > DEBIAN/postinst
chmod 0755 DEBIAN/postinst

mkdir -p usr/share/gcodeeditor
cp ../../src/main/resources/com/groomble/gcodeeditor/resources/GCodeEditorIcon.png usr/share/gcodeeditor/icon.png

echo "Package: gcodeeditor
Version: $VERSION
Section: base
Priority: optional
Architecture: all
Depends: default-jre
Maintainer: Groomblecom <groomblecom@gmail.com>
Installed-Size: `du -s . | cut -f1`
Description: Rep-Rap Flavored GCode Editor
 A simple but useful GUI-based RepRap-Flavored 
 GCode editor, ideal for tweaking files before
 they go to your 3D printer. This program is
 meant make it easy to tweak the output of your
 slicer program, not to replace the slicer.
 Written using Java with no libraries beyond
 Swing, allowing it to run practically 
 everywhere." > DEBIAN/control

cd ..
fakeroot dpkg-deb --build gcodeeditor$VERSION
rm -rf gcodeeditor$VERSION
