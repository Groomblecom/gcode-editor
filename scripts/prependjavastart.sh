#!/bin/bash
cp target/gcodeeditor.jar target/temp.jar
echo '#!/bin/sh

exec java -jar "$0" "$@"


' | cat - target/temp.jar > target/gcodeeditor.jar
rm -f target/temp.jar
chmod +x target/gcodeeditor.jar
