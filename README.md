# GCode Editor
*A simple but useful GUI-based RepRap-Flavored GCode editor, ideal for tweaking files before they go to your 3D printer.*


This program makes it easy to tweak the output of your slicer program, not meant to replace the slicer.
The use-case which prompted its creation was needing to tweak the GCode of a solder stencil, since default slicers don't do very well with thin objects needing some fine details.
It renders the GCode well enough that you don't need to have memorized the [spec](http://reprap.org/wiki/G-code), though some knowledge can help.
You do need to understand that it will cheerfully let you do some things are not physically possible (printing the same line twice on the same layer, etc.)

I wrote it using Java with no libraries beyond Swing, allowing it to run practically everywhere.
As such, it is also pretty modular and people are welcome to contribute.
The code has javadoc comments, which you can browse [on the wiki](https://bitbucket.org/Groomblecom/gcode-editor/wiki/genlinks.md).
Those are autogenerated from the source, so they are up to date with the master branch.

## Installation
If you go to the downloads page (on the left toolbar in bitbucket), there is a .jar and a .deb.
The jarfile is a double-clickable executable (and can be run as ./gcodeeditor.jar on *nixes).
Just download it, put it somewhere you'll remember (Desktop or Documents, for example), and double-click it.
It's tested to work on with Java 7 (OpenJDK), and will likely run on most java installations out there.

For Debian-derived OSes (i.e. Ubuntu), the .deb file is preferable, because it associates .gcode files with the program, installs it for all users, and makes it easy to find via the dash or list of installed programs.
You can download the .deb and run `dpkg -i gcodeeditor.deb` (replacing gcodeeditor.deb with the name of the file you downloaded).
It will prompt for authentication, then run for a moment, and then it's done.
The jarfile still works on Debian-derived OSes, it just doesn't offer as many features.

## Note about the command line
This program accepts a single command-line argument: the path to a gcode file.
If you install via the .deb, the executable is placed in the path so that you can open files like `gcodeeditor /path/to/file.gcode`.
This might be useful if someone wanted to automatically open GCode files for tweaking as a call from a larger program.